export const projectStorageCountResponse = {
  data: {
    project: {
      id: 'gid://gitlab/Project/20',
      statistics: {
        buildArtifactsSize: 400000,
        lfsObjectsSize: 4800000,
        packagesSize: 3800000,
        repositorySize: 39000000,
        snippetsSize: 0,
        storageSize: 39930000,
        uploadsSize: 0,
        wikiSize: 300000,
        __typename: 'ProjectStatistics',
      },
      __typename: 'Project',
    },
  },
};
